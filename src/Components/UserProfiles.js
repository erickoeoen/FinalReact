
import React, { useState, useEffect, useMemo } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import axios from "axios";
import { useForm, watch } from "react-hook-form";
import Axios from 'axios';

   
function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

export default function Profile(props) {
	
  const initialFormState = { 
	  name: '',
      username: '',
      email: '',
      roles: [
        {
          id: '',
          name: '',
          user_roles: {
            userId: '',
            roleId: ''
          }
        }
      ]
	}

const [ users, setUsers ] = useState("")

useMemo(() => {
        const fetchData = async () => {
          const result = await Axios({
            method: 'get', //you can set what request you want to be
            url: `http://localhost:4001/api/users/${sessionStorage.getItem('USERID')}`,
            headers: {"Authorization": sessionStorage.getItem('TOKEN')}
          });
          //console.log(result.data.user)
          //var temp =(result.data.user.slice())
          //console.log(temp)
          
          setUsers(result.data.user);
        };
        fetchData();
        //console.log(data);
      }, []);
      console.log(users.name);
    // CRUD operations
 
  const classes = useStyles();
  const { register, handleSubmit, setValue, errors, reset } = useForm({
	reValidateMode: 'onChange',
	defaultValues: {
    name: "name",
    username: "username",
    email: "email@email.com",
    password: ""
  }}
	
  );
  const onSubmit = (data, e) => {
	  
    e.preventDefault();
    console.log(data);

    axios
      .put("http://localhost:4001/api/users/edit/", data, {
        headers: {
          Authorization: sessionStorage.getItem("TOKEN")
        }
      })
      .then(function(response) {
        console.log(response)
        if(response.status === 201)
        {
//			useMemo(() => {
				const fetchData = async () => {
				  const result = await Axios({
					method: 'get', //you can set what request you want to be
					url: `http://localhost:4001/api/users/${sessionStorage.getItem('USERID')}`,
					headers: {"Authorization": sessionStorage.getItem('TOKEN')}
				  });
				  //console.log(result.data.user)
				  //var temp =(result.data.user.slice())
				  //console.log(temp)
				  
				  setUsers(result.data.user);
				};
				fetchData();
				//console.log(data);
//			  }, []);
			  console.log(users.name);
			
        alert(response.data.status);
        //window.location="http://localhost:3000/userpanel/profile";
        }
        //console.log(response.data);
        // sessionStorage.setItem('TOKEN', response.data.accessToken);
        
      })
      .catch(function(error) {
        //console.log();
        alert(error.response.data.reason)
        
      });
    
  };

   // Similar to componentDidMount and componentDidUpdate:
   useEffect(() => {
    // you can do async server request and fill up form
    
  },);
  
  return (
		
    <Container component="main" maxWidth="xs">
      <p align="center">
      <img src="http://agrobiodiversityplatform.org/files/2017/03/profile-icon-9.png" height="100px" width="100"></img>
      </p>
       {setValue('name', users.name)}
       {setValue('username', users.username)}
       {setValue('email', users.email)}
       
        <form
          onSubmit={handleSubmit(onSubmit)}
          noValidate
        >
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                defaultValue={users.name}
                
                autoComplete="fname"
                name="name"
                variant="outlined"
                required
                fullWidth
                id="firstName"
                label="Name"
                autoFocus
                error={!!errors.name}
                inputRef={register({
                  required: true
                })}
              />
              {errors.name && "name required"}
              <br />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                disabled="true"
                variant="outlined"
                required
                fullWidth
                id="lastName"
                label="Username"
                name="username"
                // value="v- usernamecccccccc"
                autoComplete="lname"
                error={!!errors.username}
                inputRef={register({
                  required: true,
                  minLength: 3, 
                  maxLength: 12
                })}
              />
               {errors.username && "username required, minimal 3 character, maximal 12 character"}
              <br />
            </Grid>
            <Grid item xs={12}>
              <TextField
               
                disabled="true"
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                // value="cccccc@email.com"
                autoComplete="email"
                error={!!errors.email}
                inputRef={register({
                  required: true,
                  pattern: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                })}
              />
              {errors.email && "Invalid email address"}
              <br />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Confirm Password"
                type="password"
                id="password"
                autoComplete="current-password"
                // value="password"
                error={!!errors.password}
                inputRef={register({
                  required: true
                })}
              />
              {errors.password && "name required"}
              <br />
            </Grid>
            <Grid item xs={12}>
            <div style={{ display: 'none' }}>
                                <select name='roles' ref={register({ required: true })} multiple>
                                    <option value='user' selected='selected'></option>
                                </select>
                            </div>
              <br />
            
            </Grid>
           </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
           Update Users
          </Button>
        </form>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>
  );
}
