import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Container from "@material-ui/core/Container";
import Button from '@material-ui/core/Button';
import Axios from 'axios';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});


const handleChange = event => {
	 const temp = event.target.value;
	 const roleId =  temp.split(",");  
     console.log(roleId)
     console.log(roleId[1])
     const data = {
		 roleId:roleId[1]
     }
     console.log(data);
      Axios
      .put(`http://localhost:4001/api/users/roles/${roleId[0]}`, data, {
        headers: {
          Authorization: sessionStorage.getItem("TOKEN")
        }
      })
      .then(function(response) {
        console.log(response)
        if(response.status === 201)
        {
        alert(response.data.message);
        }
        //console.log(response.data);
        // sessionStorage.setItem('TOKEN', response.data.accessToken);
      })
      .catch(function(error) {
        //console.log(error);
      });
    
  };
     
  
  
const UserTable = props => (

<Container component="main">
 <Button variant="contained" color="primary" href="/adminpanel/adminuseradd"> Add Users </Button>
 <div>

 <br />
   <TableContainer component={Paper}>
      <Table size="small" aria-label="a dense table">
        <TableHead>
          <TableRow>
            {/* <TableCell align="right">id</TableCell> */}
            <TableCell align="right">Name</TableCell>
           
            <TableCell align="right">Username</TableCell>
            <TableCell align="right">Email</TableCell>
            <TableCell align="right">Set Roles</TableCell>
            {/* <TableCell align="right">Actions</TableCell> */}
          </TableRow>
        </TableHead>
        <TableBody>
           {props.users.length > 0 ? (
			    props.users.map(user => (
            <TableRow key={user.id}>
              {/* <TableCell component="th" scope="row">
                {user.id}
              </TableCell> */}
              <TableCell align="right">{user.name}</TableCell>
              <TableCell align="right">{user.username}</TableCell>
              <TableCell align="right">{user.email}</TableCell>
              <TableCell align="right">
              <select className="form-control form-control-sm w-50"
                                onChange={handleChange}
                              >
                                <option value={user.id+","+user.roles[0].id}>{user.roles[0].name}</option>
                                <option value={user.roles[0].id === 1 ? user.id+","+2 : user.id+","+1}>
                                  {user.roles[0].name === "ADMIN" ? "USER" : "ADMIN"}
                                </option>
                              </select>
              </TableCell>
              
      
              <TableCell align="right">
{/*                 
              <Button
              variant="contained" color="secondary"
                 
                onClick={() => {
                  props.editRow(user)
                  window.scrollTo(0, 0);
                }}
                className="button muted-button"
              >
                Edit
              </Button> */}
              {/* <Button
				variant="contained" 
				color="primary"
                onClick={() => props.deleteUser(user.id)}
                className="button muted-button"
              >
                Delete
              </Button> */}
              </TableCell>
            </TableRow>
             ))
      ) : (
       <TableRow>
          <td colSpan={3}>No users</td>
        </TableRow>
      )}
          
        </TableBody>
      </Table>
    </TableContainer>
  </div>
  </Container>
)

export default UserTable
