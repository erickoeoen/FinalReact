import React, { useState, Fragment, useMemo } from 'react'
import AddUserForm from './AdminUsersAdd'
import EditUserForm from './AdminUsersEdit'
import UserTable from './AdminUsersView'
import Axios from 'axios';


const UserCRUD = () => {

	const initialFormState = { 
	  name: '',
      username: '',
      email: '',
      roles: [
        {
          id: '',
          name: '',
          user_roles: {
            userId: '',
            roleId: ''
          }
        }
      ]
	}

	// Setting state
	const [ users, setUsers ] = useState("")
	const [ currentUser, setCurrentUser ] = useState(initialFormState)
	const [ editing, setEditing ] = useState(false)

    useMemo(() => {
        const fetchData = async () => {
          const result = await Axios({
            method: 'get', //you can set what request you want to be
            url: 'http://localhost:4001/api/users/',
            headers: {"Authorization": sessionStorage.getItem('TOKEN')}
          });
          //console.log(result.data.user)
          //var temp =(result.data.user.slice())
          //console.log(temp)
          
          setUsers(result.data.user);
        };
        fetchData();
        //console.log(data);
      }, []);
      //console.log(users);
    // CRUD operations
    
	const addUser = user => {
		 user.id = users.length + 1
		 setUsers([ ...users, user ])
	}

	const deleteUser = id => {
		console.log(id)
		 const fetchData = async () => {
          const result = await Axios({
            method: 'delete', //you can set what request you want to be
            url: `http://localhost:4001/api/users/${id}`,
            headers: {"Authorization": sessionStorage.getItem('TOKEN')}
          });
          console.log(result.data)
          //var temp =(result.data.user.slice())
          //console.log(temp)
 
        };
        fetchData();
		setEditing(false)
		setUsers(users.filter(user => user.id !== id))
	}

	const updateUser = (id, updatedUser) => {
		
		setEditing(false)
		setUsers(users.map(user => (user.id === id ? updatedUser : user)))
	}

	const editRow = user => {
		console.log(user)		
		setEditing(true)
		setCurrentUser({ id: user.id, name: user.name, username: user.username, email: user.email, roles:user.roles })
	}

	return (
		<div className="container">
			<div className="flex-row">
				<div className="flex-large">
					{editing ? (
						<Fragment>
							<h2>Edit user</h2>
							<EditUserForm
								editing={editing}
								setEditing={setEditing}
								currentUser={currentUser}
								updateUser={updateUser}
							/>
						</Fragment>
					) : (
						<Fragment>
						{/*	<h2>Add user</h2>
							<AddUserForm addUser={addUser} /> */}
						</Fragment>
					)}
				</div>
				<div className="flex-large">
					<UserTable users={users} editRow={editRow} deleteUser={deleteUser} />
				</div>
			</div>
		</div>
	)
}

export default UserCRUD
