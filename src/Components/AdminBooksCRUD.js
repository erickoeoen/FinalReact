import React, { forwardRef, useMemo } from 'react';
import axios from 'axios';
import MaterialTable from 'material-table';
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import Container from "@material-ui/core/Container";

const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
};

export default function MaterialTableDemo(props) {
    const [state, setState] = React.useState({
        columns: [
            { title: 'id', field: 'id', type: 'numeric' },
            { title: 'title', field: 'title'},
            { title: 'author', field: 'author' },
            { title: 'published_date', field: 'published_date', type: 'date' },
            { title: 'pages', field: 'pages', type: 'numeric' },
            { title: 'language', field: 'language' },
            { title: 'publisher_id', field: 'publisher_id' },

        ],
        data: [
            {
                id: '',
                title: '',
                author: '',
                published_date: '',
                pages: '',
                language: '',
                publisher_id: '',
            }
        ],
    });

    useMemo(() => {
        const fetchData = async () => {
            const result = await axios({
                method: 'get', //you can set what request you want to be
                url: 'http://localhost:4001/api/books/',
                headers: { "Authorization": sessionStorage.getItem('TOKEN') }
            });

            console.log(JSON.stringify(result.data.data))
            var temp = {
                columns: [
                    { title: 'id', field: 'id', type: 'numeric', hidden:true},
                    { title: 'title', field: 'title' },
                    { title: 'author', field: 'author' },
                    { title: 'published_date', field: 'published_date', type: 'date' },
                    { title: 'pages', field: 'pages', type: 'numeric' },
                    { title: 'language', field: 'language' },
                    { title: 'publisher_id', field: 'publisher_id' },

                ],
                data:
                    result.data.data
                ,
            }
            setState(temp);

            console.log(temp)
        };
        fetchData();
        //console.log(data.id);
    }, []);
    return (
        <Container component="main">
            <MaterialTable
                icons={tableIcons}
                title="Books Data"
                columns={state.columns}
                data={state.data}
                editable={
                    {
                    onRowAdd: newData =>
                        new Promise(resolve => {
                            setTimeout(() => {
                                resolve();
                                setState(prevState => {
                                    const data = [...prevState.data];
                                    if(!newData.title)
                                    {
                                        alert('Title required')
                                    }
                                    if(!newData.author)
                                    {
                                        alert('Author required')
                                    }
                                    if(!newData.published_date)
                                    {
                                        alert('published date required')
                                    }
                                    if(!newData.language)
                                    {
                                        alert('language required')
                                    }
                                    if(!newData.publisher_id)
                                    {
                                        alert('publisher id required')
                                    }
                                    else{
                                    axios
                                        .post("http://localhost:4001/api/books", newData, {
                                            headers: {
                                                Authorization: sessionStorage.getItem("TOKEN")
                                            }
                                        })
                                        .then(function (response) {
                                            console.log(response);
                                            if (response.status === 201) {
                                            }
                                            console.log(response.data);
                                            // sessionStorage.setItem('TOKEN', response.data.accessToken);
                                        })
                                        .catch(function (error) {
                                            console.log(error);
                                        });
                                    data.push(newData);
                                    }
                                    console.log(newData)
                                    console.log("add data yah")
                                    return { ...prevState, data };
                                });
                            }, 600);
                        }),


                    onRowUpdate: (newData, oldData) =>
                        new Promise(resolve => {
                            setTimeout(() => {
                                resolve();
                                if (oldData) {
                                    setState(prevState => {
                                        const data = [...prevState.data];
                                        if(!newData.title)
                                        {
                                            alert('Title required')
                                        }
                                        if(!newData.author)
                                        {
                                            alert('Author required')
                                        }
                                        else{
                                        axios.put(`http://localhost:4001/api/books/${oldData.id}`, newData,
                                            {
                                                headers: {
                                                    "Authorization": sessionStorage.getItem('TOKEN')
                                                }
                                            }
                                        )
                                            .then(response => {
                                                console.log("Res : " + response.data.message);
                                            })
                                            .catch(function (error) {
                                                console.log("cacth error" + error);
                                            });
                                        data[data.indexOf(oldData)] = newData;
                                        }
                                        console.log("Edit Yah")
                                        console.log(JSON.stringify(newData))
                                        return { ...prevState, data };
                                    });
                                }
                            }, 600);
                        }),

                    onRowDelete: oldData =>
                        new Promise(resolve => {
                            setTimeout(() => {
                                resolve();
                                setState(prevState => {
                                    
                                    const data = [...prevState.data];
                                    console.log(oldData.id)
                                    axios.delete(`http://localhost:4001/api/books/${oldData.id}`,
                                        {
                                            headers: {
                                                "Authorization": sessionStorage.getItem('TOKEN')
                                            }
                                        }
                                    )
                                        .then(res => {
                                            console.log(res);
                                            console.log(res.data);
                                        });
                                    data.splice(data.indexOf(oldData), 1);
                                    console.log("Detele yah")
                                    console.log(data.indexOf(oldData.id))
                                    return { ...prevState, data };
                                });
                            }, 600);
                        }),
                }}
            />
        </Container>
    );
}