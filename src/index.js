import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import SignUp from './Components/SignUp';
import AdminBooksView from './Components/AdminBooksCRUD';
import { Route, Link, BrowserRouter as Router } from "react-router-dom";
import NotFound from "./NotFound";
import AdminPanel from "./Components/AdminPanel";
import UserPanel from "./Components/UserPanel";
import UserEdit from "./Components/AdminUsersEdit";
import AdminUserAdd from "./Components/AdminUsersAdd";
import AdminUserView from "./Components/AdminUsersCRUD";
import AdminBook from "./Components/AdminBooksCRUD";
import BooksCard from "./Components/BooksCard";
import UserBooksDetail from "./Components/UserBooksDetail";
import UserProfiles from "./Components/UserProfiles";
import AdminGreetings from "./Components/AdminHome";
import UserHome from "./Components/UserHome";


const routing = (
    
    <Router>
        <Route exact path="/" component={App} />
        <Route exact path="/signup" component={SignUp} />
        <Route path="/adminbooksview" component={AdminBooksView} />
        <Route path="/adminpanel" component={AdminPanel} />
        <Route path="/adminpanel/home" component={AdminGreetings} />
        <Route path="/adminpanel/managebook" component={AdminBook} />
        <Route path="/adminpanel/adminuseradd" component={AdminUserAdd} />
        <Route path="/adminpanel/adminuserview" component={AdminUserView} />
        <Route path="/userpanel/" component={UserPanel} />
        <Route path="/userpanel/home" component={UserHome} />
        <Route path="/userpanel/profile" component={UserProfiles} />
        <Route path="/adminpanel/profile" component={UserProfiles} />
        <Route path="/userpanel/books" component={BooksCard} />
        <Route path="/userpanel/booksid/:id?" component={UserBooksDetail} />
        <Route component={NotFound} />
    </Router>
);

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
