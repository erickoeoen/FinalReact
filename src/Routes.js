import React from "react";
import App from "./App";
import SignUp from "./Components/SignUp";
import { Route, Link, BrowserRouter as Router } from "react-router-dom";

const Routes = (
    <Router>
        <Route exact path="/" component={App} />
        <Route exact path="/signup" component={SignUp} />
    </Router>
);

export default Routes