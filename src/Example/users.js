import React, { useState, useMemo } from 'react';
import axios from 'axios';

function FetchTest() {
  const [data, setData] = useState({
      name: '',
      username: '',
      email: '',
      roles: [
        {
          id: '',
          name: '',
          user_roles: {
            userId: '',
            roleId: ''
          }
        }
      ]

  });

  useMemo(() => {
    const fetchData = async () => {
      const result = await axios({
        method: 'get', //you can set what request you want to be
        url: 'http://localhost:4001/api/users/',
        headers: {"Authorization": sessionStorage.getItem('TOKEN')}
      });
      //console.log(result.data.user)
      setData(result.data.user[0]);
    };
    fetchData();
    //console.log(data);
  }, []);
  console.log(data);
  //const mapping = data.map((data) => data.name )
  //console.log(mapping)
  return (
    <div>
   <ul>
       {console.log(data.name)}
  </ul>


</div>
    
  );
}

export default FetchTest;
