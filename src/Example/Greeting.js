import React from "react";

const greetings = props => (
  <h4 className="bg-primary text-white text-center p-2">{props.name}</h4>
);

export default greetings