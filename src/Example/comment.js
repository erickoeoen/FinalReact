import React, { Component } from "react";
import { List, Avatar, Input, Button, Switch } from "antd";

const data = [
  {
    avatar: "https://api.adorable.io/avatars/96/apple@adorable.png",
    username: "Apple Seed",
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque, inventore laboriosam!"
  },
  {
    avatar: "https://api.adorable.io/avatars/96/banana@adorable.png",
    username: "Banana Long",
    description:
      "Iusto ex ad placeat dolor tempora dolores provident hic, modi voluptatem deserunt possimus, architecto dignissimos quod fugiat enim iste, corporis neque numquam."
  },
  {
    avatar: "https://api.adorable.io/avatars/96/clementine@adorable.png",
    username: "Clementine Sour",
    description:
      "Ullam unde accusamus laboriosam enim, quam, sit ipsum amet ex repudiandae velit ipsa voluptatibus quasi impedit cupiditate, obcaecati delectus exercitationem iure quas."
  },
  {
    avatar: "https://api.adorable.io/avatars/96/dates@adorable.png",
    username: "Dates Dune",
    description:
      "Modi porro officia nesciunt cumque, debitis eum accusamus expedita, adipisci autem quos consectetur repudiandae inventore voluptas voluptate minus! Natus ducimus quisquam ipsa."
  }
];

class CommentList extends Component {
  state = {
    data,
    add: {
      avatar: "",
      username: "",
      description: "",
      checked: true
    }
  };

  onInputChange = (e, type) => {
    const add = { ...this.state.add };
    add[type] = e.target.value;
    this.setState({ add });
  };

  onAddContact = () => {
    const { data, add } = this.state;

    if (!add.username) return;

    add.avatar = add.checked
      ? `https://api.adorable.io/avatars/96/${add.username.replace(
          / +/,
          ""
        )}@adorable.png`
      : add.username.replace(/ +/, " ");
    add.username = add.username
      .split(" ")
      .map(str => str[0] && str[0].toUpperCase() + str.slice(1))
      .join(" ");
    add.description =
      add.description[0].toUpperCase() + add.description.slice(1);

    this.setState({
      data: [...data, add]
    });
  };

  onSwitchOn = checked => {
    const add = { ...this.state.add };
    add.checked = checked;
    this.setState({ add });
  };

  render() {
    const { data, add } = this.state;
    const { username, description } = add;

    return (
      <div className="CommentList">
        <List
          itemLayout="horizontal"
          dataSource={data}
          renderItem={item => (
            <List.Item>
              {/* <List.Item.Meta
                avatar={<Avatar src={item.avatar} />}
                title={item.username}
                description={item.description}
              /> */}
              <div>
                {item.username}
              </div>
            </List.Item>
          )}
          style={{
            width: "100%"
          }}
        />
        <form className="CommentList__form" autoComplete="off">
          <div className="CommentList__input">
            <label htmlFor="name">Name</label>
            <Input
              value={username}
              placeholder="Your beautiful name here"
              id="name"
              onChange={e => this.onInputChange(e, "username")}
            />
          </div>
          <div className="CommentList__input">
            <label htmlFor="description">Description</label>
            <Input
              value={description}
              placeholder="Your beautiful description here"
              id="description"
              onChange={e => this.onInputChange(e, "description")}
            />
          </div>
          <div className="CommentList__add">
            <Button
              onClick={this.onAddContact}
              style={{ marginRight: "1em" }}
              disabled={!add.username}
            >
              Add
            </Button>
            <Switch
              defaultChecked
              checkedChildren="👦"
              onChange={this.onSwitchOn}
              style={{ minWidth: "48px" }}
            />
          </div>
        </form>
      </div>
    );
  }
}

export default CommentList;
