import React, { useState, useEffect } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import axios from "axios";
import { useForm, watch } from "react-hook-form";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

export default function SignUp() {
 
  const classes = useStyles();
  const { register, handleSubmit, setValue, errors } = useForm();
  const onSubmit = (data, e) => {
    e.preventDefault();
    alert(JSON.stringify(data, null, 2));
    console.log(data);

    axios
      .post("http://localhost:4001/api/auth/signup", data, {
        headers: {
          Authorization: sessionStorage.getItem("TOKEN")
        }
      })
      .then(function(response) {
        console.log(response)
        if(response.status === 201)
        {
        alert(response.data.status);
        window.location = "/";
        }
        //console.log(response.data);
        // sessionStorage.setItem('TOKEN', response.data.accessToken);
      })
      .catch(function(error) {
        console.log(error);
      });
    //props.history.push("/");
  };

  const [values, setroles] = useState({
    selectedOption: []
  });

  const handleMultiChange = selectedOption => {
    setValue("roles", selectedOption);
    setroles({ selectedOption });
  };

  useEffect(() => {
    register({ name: "roles" });
  }, []);

  const options = [
    "ADMIN",
    { label: "USER" },
    { label: "PM" }
  ];

  const roles = 
  {
    roles :["ADMIN"] 
  }
  
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        {console.log(roles)};
        <form
          className={classes.form}
          onSubmit={handleSubmit(onSubmit)}
          noValidate
        >
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="fname"
                name="name"
                variant="outlined"
                required
                fullWidth
                id="firstName"
                label="Name"
                // value="namesss"
                autoFocus
                error={!!errors.name}
                inputRef={register({
                  required: true
                })}
              />
              {errors.name && "name required"}
              <br />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="lastName"
                label="Username"
                name="username"
                // value="v- usernamecccccccc"
                autoComplete="lname"
                error={!!errors.username}
                inputRef={register({
                  required: true
                })}
              />
              {errors.username && "username required"}
              <br />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                // value="cccccc@email.com"
                autoComplete="email"
                error={!!errors.email}
                inputRef={register({
                  required: true,
                  pattern: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                })}
              />
              {errors.email && "Invalid email address"}
              <br />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                // value="password"
                error={!!errors.password}
                inputRef={register({
                  required: true
                })}
              />
              {errors.password && "name required"}
              <br />
            </Grid>
            <Grid item xs={12}>

            {/* <lable className="reactSelectLabel">React select</lable>
            <Select
            className="reactSelect"
            name="filters"
            placeholder="Filters"
            value="USER"
            selected="selected"
            options={options}
            onChange={handleMultiChange}
            isMulti
            /> */}
            <div style={{ display: 'none' }}>
                                <select name='roles' ref={register({ required: true })} multiple>
                                    <option value='user' selected='selected'></option>
                                </select>
                            </div>
              {errors.roles && "name required"}
              <br />
            
            </Grid>
           </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign Up
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link href="/" variant="body2">
                Already have an account? Sign in
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>
  );
}