import React from "react";
import Axios from "axios";
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Container from "@material-ui/core/Container";

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

export default class UsersList extends React.Component {
    //constructor(props)
    state = {
        //   super(props);
        book: []

    };


    componentDidMount() {
        if (sessionStorage.getItem("TOKEN") === null) {
            alert("you must login to show this page")
            window.location = "/login";
        }
        //this.timerID = setInterval(() => this.updateClock(), 1000);
        // Axios.get("http://localhost:4000/api/books/").then(res => {
        //   const book = res.data;
        //   this.setState({ book : book.data });
        //   console.log(res.status)
        // });
        Axios.get(
            'http://localhost:4001/api/users',
            {
                headers: {
                    "Authorization": sessionStorage.getItem('TOKEN')
                }
            }
        )
            .then((response) => {
                var book = response.data.user;
                console.log(book);
                
                    this.setState({ book: book });
                
            }
                ,
                (error) => {
                    //console.log(JSON.stringify(error.response.data.message))
                    // console.log(response)
                    // var status = error.response

                }
            );
    }

    // componentWillMount(){
    //     clearInterval(this.timerID)
    // }

    // updateClock()
    // {
    //     this.setState({
    //         date: new Date()
    //     })
    // }

    render() {
        return (

            <Container component="main">
            <TableContainer component={Paper}>
                <Table aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell align="right">id</TableCell>
                            <TableCell align="right">name</TableCell>
                            <TableCell align="right">username</TableCell>
                            <TableCell align="right">email</TableCell>
                            
                            <TableCell align="right">edit</TableCell>
                            <TableCell align="right">delete</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.state.book.map(books => (
                            <TableRow key={books.id}>
                                <TableCell component="th" scope="row">
                                    {books.id}
                                </TableCell>
                                <TableCell align="right">{books.name}</TableCell>
                                <TableCell align="right">{books.username}</TableCell>
                                <TableCell align="right">{books.email}</TableCell>
                                <TableCell align="right">Edit</TableCell>
                                <TableCell align="right">Delete</TableCell>
                                
                               </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            </Container>

        );
    }
}
